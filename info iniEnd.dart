import 'dart:async';

import 'package:codelab_timetracker/PageIntervals.dart';
import 'package:codelab_timetracker/pageCreateProject.dart';
import 'package:codelab_timetracker/tree.dart' hide getTree; // the old getTree()
import 'package:codelab_timetracker/requests.dart'; // has the new getTree() that sends an http request to the server

import 'package:flutter/material.dart';


class PageActivities extends StatefulWidget {
  int id;
  PageActivities(this.id);
  @override
  _PageActivitiesState createState() => _PageActivitiesState();
}

class _PageActivitiesState extends State<PageActivities> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int id;
  Future<Tree>  futureTree;

  Timer _timer;

  static const int periodeRefresh = 2;
  // better a multiple of periode in TimeTracker, 2 seconds

  void _activateTimer() {
    _timer = Timer.periodic(Duration(seconds: periodeRefresh), (Timer t) {
      futureTree = getTree(id);
      setState(() {});
    });
  }
  @override
  void initState() {
    super.initState();
    id = widget.id; // of PageActivities
    print("START GET TREE  with id " + id.toString());
    futureTree = getTree(id);
    _activateTimer();

  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Tree>(
      future: futureTree,
      // this makes the tree of children, when available, go into snapshot.data
      builder: (context, snapshot) {
        // anonymous function
        if (snapshot.hasData) {
          return Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              title: Text(snapshot.data.root.name), //TODO FUNCION TEXTO ROOT
              actions: <Widget>[
                IconButton(icon: Icon(Icons.home),
                    onPressed: () {
                      while(Navigator.of(context).canPop()) {
                        print("pop");
                        Navigator.of(context).pop();
                      }
                      /* this works also:
  Navigator.popUntil(context, ModalRoute.withName('/'));
  */
                      PageActivities(0);
                    }),
                //TODO other actions
              ],
            ),
            body: ListView.separated(
              // it's like ListView.builder() but better because it includes a separator between items
              padding: const EdgeInsets.all(16.0),
              itemCount: snapshot.data.root.children.length,
              itemBuilder: (BuildContext context, int index) =>
                  _buildRow(snapshot.data.root.children[index], index),
              separatorBuilder: (BuildContext context, int index) =>
              const Divider(),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                // Add your onPressed code here!
                _navigateDownCreateProject();
              },
              child: Icon(Icons.add_outlined),
              backgroundColor: Colors.green,
            ),
          );
        } else if (snapshot.hasError) {
          return Text("${snapshot.error} y snapshot.hasData = ${snapshot.hasData}");
        }
        // By default, show a progress indicator
        return Container(
            height: MediaQuery.of(context).size.height,
            color: Colors.white,
            child: Center(
              child: CircularProgressIndicator(),
            ));
      },
    );
  }

  Widget _buildRow(Activity activity, int index) {

    int dur = activity.duration;

    String strDuration;
    String _infoIniEnd ;

    if (dur == null)
      strDuration = "0:00:00";// TODO revisar fomato
    else
      strDuration = Duration(seconds: activity.duration).toString().split('.').first;


    if (activity.initialDate == null){
      _infoIniEnd = "no iniciado";
    }else{
      _infoIniEnd = "\n ${activity.initialDate.toString().split('.').first}"
          "\n ${activity.finalDate.toString().split('.').first}";
    }
    // split by '.' and taking first element of resulting list removes the microseconds part
    if (activity is Project) {


      return ListTile(
        title: Text('${activity.name} '),
        subtitle: Text('Project: $_infoIniEnd' ),
        trailing: Text('$strDuration'),
        onTap: () => _navigateDownActivities(activity.id),
      );


    } else if (activity is Task) {
      Task task = activity;
      // at the moment is the same, maybe changes in the future
      Widget trailing;
      trailing = Text('$strDuration');

      SnackBar startSnack = SnackBar(
        content: Text("Task " + activity.name + " started"),
        duration: Duration(seconds: 1),
      );
      SnackBar stopSnack = SnackBar(
        content: Text("Task " + activity.name + " stopped"),
        duration: Duration(seconds: 1),
      );

      return ListTile(
        title: Text('${activity.name}'),
        subtitle: Text('${"Task  $_infoIniEnd" }'),
        leading: IconButton(icon: Icon(Icons.play_arrow), onPressed: () => _checkStartStop(stopSnack, startSnack, activity)),
        trailing: trailing,//Icon(Icons.keyboard_arrow_right),
        onTap: () => _navigateDownIntervals(activity.id),
      );
    }
  }

  void _refresh() async {
    futureTree = getTree(id); // to be used in build()
    setState(() {});
  }

  void _navigateDownActivities(int childId) {
    _timer.cancel();   // we can not do just _refresh() because then the up arrow doesnt appear in the appbar
    Navigator.of(context)
        .push(MaterialPageRoute<void>(
      builder: (context) => PageActivities(childId),
    )).then( (var value) {
      _activateTimer();
      _refresh();
    });
  }

  void _navigateDownIntervals(int childId) {
    _timer.cancel();
    Navigator.of(context)
        .push(MaterialPageRoute<void>(
      builder: (context) => PageIntervals(childId),
    )).then( (var value) {
      _activateTimer();
      _refresh();
    });
  }

  void _navigateDownCreateProject() {
    _timer.cancel();   // we can not do just _refresh() because then the up arrow doesnt appear in the appbar
    Navigator.of(context)
        .push(MaterialPageRoute<void>(
      builder: (context) => PageCreateProject(),
    )).then( (var value) {
      _activateTimer();
      _refresh();
    });
  }

  @override
  void dispose() {
    // "The framework calls this method when this State object will never build again"
    // therefore when going up
    _timer.cancel();
    super.dispose();
  }

  void _checkStartStop(SnackBar stopSnack, SnackBar startSnack, Activity activity){
    if ((activity as Task).active) {
      stop(activity.id);
      _scaffoldKey.currentState.showSnackBar(stopSnack);
      _refresh();

    } else {
      start(activity.id);
      _scaffoldKey.currentState.showSnackBar(startSnack);
      _refresh();
    }
  }

}
