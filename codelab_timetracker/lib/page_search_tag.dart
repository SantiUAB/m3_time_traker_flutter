import 'package:flutter/material.dart';
import 'generated/l10n.dart';

class PageSearchTag extends StatefulWidget {

  @override
  _PageSearchTagState createState() => _PageSearchTagState();
}

class _PageSearchTagState extends State<PageSearchTag> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
            S.of(context).search_tag), //TODO modificar dependiendo de tarea/proyecto
      ),
      body: Center(
        child:
        Padding(
          padding: const EdgeInsets.all(16.0),
        child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const SizedBox(height: 30),
          TextFormField(
            onChanged: (value) {},
            decoration: InputDecoration(
              labelText: S.of(context).search_tag_ask,
            ),
          ),
          //title: snapshot.data.root.name == "ROOT" ? Text("TimeTracker") : Text(snapshot.data.root.name),
          RaisedButton(
            onPressed: () {
              while (Navigator.of(context).canPop()) {
                print("pop");
                Navigator.of(context).pop();
              }
              //PageActivities(0);
            },
            child: Text(S.of(context).search, style: TextStyle(fontSize: 20)),
          ),
          const SizedBox(height: 30),
        ],
      )
      ),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Container(
          height: 50.0,
        ),
      ),
    );
  }
}