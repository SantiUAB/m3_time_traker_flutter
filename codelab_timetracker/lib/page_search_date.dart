import 'package:flutter/material.dart';
import 'package:date_time_picker/date_time_picker.dart';

import 'package:codelab_timetracker/generated/l10n.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  GlobalKey<FormState> _oFormKey = GlobalKey<FormState>();
  TextEditingController _controller1;
  TextEditingController _controller2;
  TextEditingController _controller3;
  TextEditingController _controller4;
  //String _initialValue;
  String _valueChanged1 = '';
  String _valueToValidate1 = '';
  String _valueSaved1 = '';
  String _valueChanged2 = '';
  String _valueToValidate2 = '';
  String _valueSaved2 = '';
  String _valueChanged3 = '';
  String _valueToValidate3 = '';
  String _valueSaved3 = '';
  String _valueChanged4 = '';
  String _valueToValidate4 = '';
  String _valueSaved4 = '';

  @override
  void initState() {
    super.initState();

    //_initialValue = DateTime.now().toString();
    _controller1 = TextEditingController(text: DateTime.now().toString());
    _controller2 = TextEditingController(text: DateTime.now().toString());
    _controller3 = TextEditingController(text: DateTime.now().toString());

    String lsHour = TimeOfDay.now().hour.toString().padLeft(2, '0');
    String lsMinute = TimeOfDay.now().minute.toString().padLeft(2, '0');
    _controller4 = TextEditingController(text: '$lsHour:$lsMinute');

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        title: Text(S.of(context).search_interval),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(left: 20, right: 20, top: 10),
        child: Form(
          key: _oFormKey,
          child: Column(
            children: <Widget>[
              /* subtitle: Text('${"Proyecto : $_infoIniEnd"} ',
            style: new TextStyle(fontSize: 15.0),)*/
              new Container(
                  margin: const EdgeInsets.only(top:10.0),
                  child: Text(S.of(context).initial_date_time,
                    style: new TextStyle(fontSize: 20.0),)
              ),
              DateTimePicker(
                type: DateTimePickerType.dateTimeSeparate,
                dateMask: 'd MMM, yyyy',
                controller: _controller1,
                //initialValue: _initialValue,
                firstDate: DateTime(2020),
                lastDate: DateTime(2100),
                icon: Icon(Icons.event),
                dateLabelText: S.of(context).date,
                timeLabelText: S.of(context).hour,
                //use24HourFormat: false,
                //locale: Locale('pt', 'BR'),
                selectableDayPredicate: (date) {
                  if (date.weekday == 6 || date.weekday == 7) {
                    return false;
                  }
                  return true;
                },
                onChanged: (val) => setState(() => _valueChanged1 = val),
                validator: (val) {
                  setState(() => _valueToValidate1 = val);
                  return null;
                },
                onSaved: (val) => setState(() => _valueSaved1 = val),
              ),
              new Container(
                  margin: const EdgeInsets.only(top:20.0),
                  child: Text(S.of(context).final_date_time,
                    style: new TextStyle(fontSize: 20.0),)
              ),
              DateTimePicker(
                type: DateTimePickerType.dateTimeSeparate,
                dateMask: 'd MMM, yyyy',
                controller: _controller2,
                //initialValue: _initialValue,
                firstDate: DateTime(2020),
                lastDate: DateTime(2100),
                icon: Icon(Icons.event),
                dateLabelText: S.of(context).date,
                timeLabelText: S.of(context).hour,
                //use24HourFormat: false,
                //locale: Locale('pt', 'BR'),
                selectableDayPredicate: (date) {
                  if (date.weekday == 6 || date.weekday == 7) {
                    return false;
                  }
                  return true;
                },
                onChanged: (val) => setState(() => _valueChanged1 = val),
                validator: (val) {
                  setState(() => _valueToValidate1 = val);
                  return null;
                },
                onSaved: (val) => setState(() => _valueSaved1 = val),
              ),
              new Container(
                  margin: const EdgeInsets.only(top:10.0),
                  child:  RaisedButton(
                    onPressed: () {
                      while (Navigator.of(context).canPop()) {
                        print("pop");
                        Navigator.of(context).pop();
                      }
                      //PageActivities(0);
                    },
                    child: Text(S.of(context).search, style: TextStyle(fontSize: 20)),
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }
}