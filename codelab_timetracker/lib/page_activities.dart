import 'dart:async';

import 'package:codelab_timetracker/page_intervals.dart';
import 'package:codelab_timetracker/page_create_activity.dart';
import 'package:codelab_timetracker/page_search_tag.dart';
import 'package:codelab_timetracker/page_search_date.dart';
import 'package:codelab_timetracker/tree.dart' hide getTree; // the old getTree()
import 'package:codelab_timetracker/requests.dart'; // has the new getTree() that sends an http request to the server
import 'package:codelab_timetracker/generated/l10n.dart';

import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

import 'page_search_date.dart';



class PageActivities extends StatefulWidget {
  int id;
  PageActivities(this.id);
  @override
  _PageActivitiesState createState() => _PageActivitiesState();

}

class _PageActivitiesState extends State<PageActivities> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int id;
  Future<Tree>  futureTree;

  Timer _timer;

  static const int periodeRefresh = 2;
  // better a multiple of periode in TimeTracker, 2 seconds

  void _activateTimer() {
    _timer = Timer.periodic(Duration(seconds: periodeRefresh), (Timer t) {
      futureTree = getTree(id);
      setState(() {});
    });
  }
  @override
  void initState() {
    super.initState();
    id = widget.id; // of PageActivities
    print("START GET TREE  with id " + id.toString());
    futureTree = getTree(id);
    _activateTimer();

  }

  Locale _userLocale;

  @override
  void didChangeDependencies() {

    Locale newLocale = Localizations.localeOf(context);

    if (newLocale != _userLocale) {
      _userLocale = newLocale;
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Tree>(
      future: futureTree,
      // this makes the tree of children, when available, go into snapshot.data
      builder: (context, snapshot) {
        // anonymous function
        if (snapshot.hasData) {
          return Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              title: snapshot.data.root.name == "ROOT" ? Text("TimeTracker") /*Text(S.of(context).appTitle)*/: Text(snapshot.data.root.name),
              actions: <Widget>[
                IconButton(icon: Icon(Icons.search, color:Colors.white), onPressed: _showPopupMenu),
                IconButton(icon: Icon(Icons.home),
                    onPressed: () {
                      while(Navigator.of(context).canPop()) {
                        print("pop");
                        Navigator.of(context).pop();
                      }
                      PageActivities(0);
                    }),

                //TODO other actions
              ],
            ),
            body: ListView.separated(
              // it's like ListView.builder() but better because it includes a separator between items
              padding: const EdgeInsets.all(16.0),
              itemCount: snapshot.data.root.children.length,
              itemBuilder: (BuildContext context, int index) =>
                  _buildRow(snapshot.data.root.children[index], index),
              separatorBuilder: (BuildContext context, int index) =>
              const Divider(),
            ),
            floatingActionButton: _getFAB(),
          );
        } else if (snapshot.hasError) {
          return Text("${snapshot.error} y snapshot.hasData = ${snapshot.hasData}");
        }
        // By default, show a progress indicator
        return Container(
            height: MediaQuery.of(context).size.height,
            color: Colors.white,
            child: Center(
              child: CircularProgressIndicator(),
            ));
      },
    );
  }

  Widget _buildRow(Activity activity, int index) {

    int dur = activity.duration;

    String strDuration;
    String _infoIniEnd ; // TODO formatear por localización

    if (dur == null)
      strDuration = "0";
    else
      strDuration = Duration(seconds: activity.duration).toString().split('.').first;

    if (activity.initialDate == null){
      (activity is Task) ? _infoIniEnd = S.of(context).not_started_task : _infoIniEnd = S.of(context).not_started_project;
    }else{

      DateTime activityInit = activity.initialDate;
      DateTime activityFinal = activity.finalDate;

      String strInitialDate = S.of(context).date_format(activityInit.month.toString(), activityInit.day.toString(), activityInit.year.toString(),
          activityInit.hour.toString(), activityInit.minute.toString(), activityInit.second.toString());

      String strFinalDate = S.of(context).date_format(activityFinal.month.toString(), activityFinal.day.toString(), activityFinal.year.toString(),
          activityFinal.hour.toString(), activityFinal.minute.toString(), activityFinal.second.toString());

      _infoIniEnd = "\n ${S.of(context).ini}: ${strInitialDate}"
          "\n ${S.of(context).end}: \t\t\t\t${strFinalDate}";
    }
    // split by '.' and taking first element of resulting list removes the microseconds part
    if (activity is Project) {
      return ListTile(
        title: Text('${activity.name}'),
        subtitle: Text('${"${S.of(context).project} : $_infoIniEnd"} ',
          style: new TextStyle(fontSize: 15.0),),
        trailing: Row (
            mainAxisSize : MainAxisSize.min,
            children: <Widget>[
        Text('$strDuration'),
          Icon(Icons.keyboard_arrow_right),
        ]),
        onTap: () => _navigateDownActivities(activity.id),
      );
    } else if (activity is Task) {
      Task task = activity;
      // at the moment is the same, maybe changes in the future
      Widget trailing;
      trailing = Text('$strDuration');

      SnackBar startSnack = SnackBar(
          content: Text(S.of(context).task_started(activity.name)),
          duration: Duration(seconds: 1),
      );
      SnackBar stopSnack = SnackBar(
          content: Text(S.of(context).task_stopped(activity.name)),
          duration: Duration(seconds: 1),
      );



      return Ink(
        color: ((activity).active) ? Colors.greenAccent: Colors.transparent,
        child: ListTile(
          title: Text('${activity.name}'),
          subtitle: Text('${"${S.of(context).task} : $_infoIniEnd"}',
            style: new TextStyle(fontSize: 15.0),),
          trailing: Row(
            mainAxisSize : MainAxisSize.min,
            children: <Widget>[
              ((activity).active) ?
              IconButton(icon: Icon(Icons.pause), onPressed: () => _checkStartStop(stopSnack, startSnack, activity)):
              IconButton(icon: Icon(Icons.play_arrow), onPressed: () => _checkStartStop(stopSnack, startSnack, activity)),
              trailing,
              Icon(Icons.keyboard_arrow_right),
            ]
          ),
          onTap: () => _navigateDownIntervals(activity.id),
        ),
      );
    }
  }

  Widget _getFAB() {
    int _counter;
    return SpeedDial(
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 22),
      backgroundColor: Colors.teal,
      visible: true,
      curve: Curves.bounceIn,
      children: [
        // FAB 1
        SpeedDialChild(
            child: Icon(Icons.folder),
            backgroundColor: Colors.teal,
            onTap: () => _navigateDownCreateProject(id, false),
            label: S.of(context).new_project,
            labelStyle: TextStyle(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 16.0),
            labelBackgroundColor: Colors.teal),
        // FAB 2
        SpeedDialChild(
            child: Icon(Icons.insert_drive_file),
            backgroundColor: Colors.teal,
            onTap: () {
              _navigateDownCreateProject(id, true);
              setState(() {
                _counter = 0;
              });
            },
            label: S.of(context).new_task,
            labelStyle: TextStyle(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 16.0),
            labelBackgroundColor: Colors.teal)
      ],
    );
  }

  void _refresh() async {
    futureTree = getTree(id); // to be used in build()
    setState(() {});
  }

  void _navigateDownActivities(int childId) {
    _timer.cancel();   // we can not do just _refresh() because then the up arrow doesnt appear in the appbar
    Navigator.of(context)
        .push(MaterialPageRoute<void>(
      builder: (context) => PageActivities(childId),
    )).then( (var value) {
      _activateTimer();
      _refresh();
    });
  }

  void _navigateDownIntervals(int childId) {
    _timer.cancel();
    Navigator.of(context)
        .push(MaterialPageRoute<void>(
      builder: (context) => PageIntervals(childId),
    )).then( (var value) {
      _activateTimer();
      _refresh();
    });
  }

  void _navigateDownCreateProject(int childId, bool isTask) {
    _timer.cancel();   // we can not do just _refresh() because then the up arrow doesnt appear in the appbar
    Navigator.of(context)
        .push(MaterialPageRoute<void>(
      builder: (context) => PageCreateProject(childId, isTask),
    )).then( (var value) {
      _activateTimer();
      _refresh();
    });
  }

  @override
  void dispose() {
    // "The framework calls this method when this State object will never build again"
    // therefore when going up
    _timer.cancel();
    super.dispose();
  }

  void _checkStartStop(SnackBar stopSnack, SnackBar startSnack, Activity activity){
    if ((activity as Task).active) {
      stop(activity.id);
      _scaffoldKey.currentState.showSnackBar(stopSnack);
      _refresh();

    } else {
      start(activity.id);
      _scaffoldKey.currentState.showSnackBar(startSnack);
      _refresh();
    }
  }

  _showPopupMenu(){
    showMenu<String>(
      context: context,
      position: RelativeRect.fromLTRB(25.0, 25.0, 0.0, 0.0),      //position where you want to show the menu on screen
      items: [
        PopupMenuItem<String>(
            child: Text(S.of(context).search_tag), value: '1'),
        PopupMenuItem<String>(
            child: Text(S.of(context).search_interval), value: '2'),
      ],
      elevation: 8.0,
    )
        .then<void>((String itemSelected) {

      if (itemSelected == null) return;

      if(itemSelected == "1"){
        _searchByTag();
      }else {
        _searchByDate();
      }
        //code here
      }

    );
  }

  void _searchByTag() {
    _timer.cancel();   // we can not do just _refresh() because then the up arrow doesnt appear in the appbar
    Navigator.of(context)
        .push(MaterialPageRoute<void>(
      builder: (context) => PageSearchTag(),
    )).then( (var value) {
      _activateTimer();
      _refresh();
    });
  }

  void _searchByDate() {
    _timer.cancel();   // we can not do just _refresh() because then the up arrow doesnt appear in the appbar
    Navigator.of(context)
        .push(MaterialPageRoute<void>(
      builder: (context) => MyHomePage(),
    )).then( (var value) {
      _activateTimer();
      _refresh();
    });
  }

}
