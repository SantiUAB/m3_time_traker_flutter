import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'tree.dart';

final http.Client client = http.Client();
// better than http.get() if multiple requests to the same server

// If you connect the Android emulator to the webserver listening to localhost:8080
//const String baseUrl = "http://10.0.2.2:8080";
//const String baseUrl = "http://localhost:8080";
//const String baseUrl = "https://6a43f280843f.ngrok.io"; // andres
//const String baseUrl = "https://50be92ce8aeb.ngrok.io"; // $ngrok http 8080 - santaigo base URL cada vez que se ejecuta
const String baseUrl = "https://11a02166c5d2.ngrok.io"; // $ngrok http 8080 - santaigo base URL cada vez que se ejecuta


// If instead you want to use a real phone, run this command in the linux terminal
//   ssh -R joans.serveousercontent.com:80:localhost:8080 serveo.net
//const String baseUrl = "https://joans.serveousercontent.com";

Future<Tree> getTree(int id) async {
  String uri = "$baseUrl/get_tree?$id";
  print(" URI getTree  " + uri + "\nSTART AWAIT");
  final response = await client.get(uri);
  print("END AWAIT");
  // response is NOT a Future because of await but since getTree() is async,
  // execution continues (leaves this function) until response is available,
  // and then we come back here
  if (response.statusCode == 200) {
    print("statusCode=$response.statusCode");
    print(response.body);
    // If the server did return a 200 OK response, then parse the JSON.
    Map<String, dynamic> decoded = convert.jsonDecode(response.body);
    return Tree(decoded);
  } else {
    // If the server did not return a 200 OK response, then throw an exception.
    print("statusCode=$response.statusCode");
    throw Exception('Failed to get children al hacer getTree');
  }
}

Future<void> start(int id) async {
  String uri = "$baseUrl/start?$id";
  final response = await client.get(uri);
  if (response.statusCode == 200) {
    print("statusCode=$response.statusCode");
  } else {
    print("statusCode=$response.statusCode");
    throw Exception('Failed to get children al start');
  }
}

Future<void> stop(int id) async {
  String uri = "$baseUrl/stop?$id";
  final response = await client.get(uri);
  if (response.statusCode == 200) {
    print("statusCode=$response.statusCode");
  } else {
    print("statusCode=$response.statusCode");
    throw Exception('Failed to get children al stop');
  }
}

Future<void> addNewProject(String pName, int id) async {
  String uri = "$baseUrl/addNewProject?$pName?$id";
  final response = await client.get(uri);
  if (response.statusCode == 200) {
    print("statusCode=$response.statusCode");
  } else {
    print("statusCode=$response.statusCode");
    throw Exception('Failed to add new project');
  }
}

Future<void> addNewTask(String tName, int id) async {
  String uri = "$baseUrl/addNewTask?$tName?$id";
  final response = await client.get(uri);
  if (response.statusCode == 200) {
    print("statusCode=$response.statusCode");
  } else {
    print("statusCode=$response.statusCode");
    throw Exception('Failed to add new task');
  }
}

