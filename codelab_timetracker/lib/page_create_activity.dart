import 'package:flutter/material.dart';
import 'package:codelab_timetracker/requests.dart';
import 'package:codelab_timetracker/generated/l10n.dart';

class PageCreateProject extends StatefulWidget {
  int id;
  bool isTask;

  PageCreateProject(this.id, this.isTask);
  //PageCreateProject();
  @override
  _PageCreateState createState() => _PageCreateState();
}

class _PageCreateState extends State<PageCreateProject> {
  int id;
  bool isTask;
  String pName = "";

  @override
  void initState() {
    super.initState();
    id = widget.id;
    isTask = widget.isTask;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: isTask == true ? Text(S.of(context).new_task) :
                                Text(S.of(context).new_project),
      ),
      body: Center(child:
      Padding(
        padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const SizedBox(height: 30),
          TextFormField(
              onChanged: (value) {
                pName = value;
              },
            decoration: InputDecoration(
                labelText: isTask == true ? S.of(context).input_task_name : S.of(context).input_project_name,
            ),
          ),
          RaisedButton(
            onPressed: isTask == true ? () => addTask() : () => addProject(),
            child: isTask == true ? Text(S.of(context).new_task, style: TextStyle(fontSize: 20)) :
                                    Text(S.of(context).new_project , style: TextStyle(fontSize: 20)),
          ),
          const SizedBox(height: 30),
        ],
      )
      ),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Container(
          height: 50.0,
        ),
      ),
    );
  }

  void addProject() {
    print(pName);
    print(id);
    addNewProject(pName, id);
    if (Navigator.of(context).canPop()) {
        print("pop");
        Navigator.of(context).pop();
    }
  }

  void addTask() {
    print(pName);
    print(id);
    addNewTask(pName, id);
    if (Navigator.of(context).canPop()) {
      print("pop");
      Navigator.of(context).pop();
    }
  }
}