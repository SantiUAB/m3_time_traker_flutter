import 'dart:async';

import 'package:codelab_timetracker/page_activities.dart';
import 'package:flutter/material.dart';
import 'package:codelab_timetracker/tree.dart' as Tree hide getTree; // to avoid collision with an Interval class in another library
import 'package:codelab_timetracker/requests.dart';
import 'package:codelab_timetracker/generated/l10n.dart';

import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

const String LOCALE_ES = 'eu_ES';
const String HOUR_MINUTE_SECOND = 'Hms';
//const String HOUR_MINUTE_SECOND = 'jms';

// to avoid collision with an Interval class in another library
class PageIntervals extends StatefulWidget {
  int id;

  PageIntervals(this.id);
  @override
  _PageIntervalsState createState() => _PageIntervalsState();
}


// to avoid collision with an Interval class in another library
class _PageIntervalsState extends State<PageIntervals> {
  int id;
  Future<Tree.Tree> futureTree;

  Timer _timer;
  static const int periodeRefresh = 2;

  void _activateTimer() {
    _timer = Timer.periodic(Duration(seconds: periodeRefresh), (Timer t) {
      futureTree = getTree(id);
      setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    id = widget.id;
    futureTree = getTree(id);
    _activateTimer();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Tree.Tree>(
      future: futureTree,
      // this makes the tree of children, when available, go into snapshot.data
      builder: (context, snapshot) {
        // anonymous function
        if (snapshot.hasData) {
          int numChildren = snapshot.data.root.children.length;
          return Scaffold(
            appBar: AppBar(
              title: Text(snapshot
                  .data.root.name),
              actions: <Widget>[
                IconButton(icon: Icon(Icons.home),
                    onPressed: () {
                      while(Navigator.of(context).canPop()) {
                        print("pop");
                        Navigator.of(context).pop();
                      }
                      /* this works also:
  Navigator.popUntil(context, ModalRoute.withName('/'));
  */
                      PageActivities(0);
                    }),
              ],
            ),
            body: ListView.separated(
              // it's like ListView.builder() but better because it includes a separator between items
              padding: const EdgeInsets.all(16.0),
              itemCount: numChildren,
              itemBuilder: (BuildContext context, int index) =>
                  _buildRow(snapshot.data.root.children[index], index, snapshot.data.root, numChildren),
              separatorBuilder: (BuildContext context, int index) =>
              const Divider(),
            ),
          );
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }
        // By default, show a progress indicator
        return Container(
            height: MediaQuery.of(context).size.height,
            color: Colors.white,
            child: Center(
              child: CircularProgressIndicator(),
            ));
      },

    );
  }

  Widget _buildRow(Tree.Interval interval, int index, Tree.Task parent, int numChildren) {
    String strDuration;

    String strInitialDate;// = interval.initialDate.toString().split('.')[0];
    // this removes the microseconds part
    String strFinalDate;// = interval.finalDate.toString().split('.')[0];

    DateTime intervInit = interval.initialDate;
    DateTime intervFinal = interval.finalDate;

    strInitialDate = S.of(context).date_format(intervInit.month.toString(), intervInit.day.toString(), intervInit.year.toString(),
                                               intervInit.hour.toString(), intervInit.minute.toString(), intervInit.second.toString());

    strFinalDate = S.of(context).date_format(intervFinal.month.toString(), intervFinal.day.toString(), intervFinal.year.toString(),
                                             intervFinal.hour.toString(), intervFinal.minute.toString(), intervFinal.second.toString());

    //TODO control temporal
    if(interval.duration == null){

      strInitialDate = interval.initialDate.toString().split('.')[0];
      strFinalDate = interval.finalDate.toString().split('.')[0];

      var iniInterval = DateTime.parse(strInitialDate);
      var endInterval = DateTime.parse(strFinalDate);


      Duration duration = iniInterval.difference(endInterval);
     

      format(Duration d) => d.toString().split('.').first.padLeft(8, "0").substring(1,8);


      strDuration = format(duration);

    }else{
      strDuration = Duration(seconds: interval.duration).toString().split('.').first;
    }

    bool last;
    if (index+1 == numChildren)
      last = true;
    else last = false;


    return Ink(
      color: (((parent).active) && last) ? Colors.greenAccent: Colors.transparent,
      child: ListTile(
        title: Text('${S.of(context).ini}:\t ${strInitialDate}'
              +'\n'+'${S.of(context).end}:\t\t\t\t\t ${strFinalDate}'),
        subtitle: Text('${"Interval"}' ,
          style: new TextStyle(fontSize: 15.0),),
        trailing: Text('$strDuration'),
      )
    );
  }

  @override
  void dispose() {
    // "The framework calls this method when this State object will never build again"
    // therefore when going up
    _timer.cancel();
    super.dispose();
  }
}
