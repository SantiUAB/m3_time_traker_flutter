// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Time tracker`
  String get appTitle {
    return Intl.message(
      'Time tracker',
      name: 'appTitle',
      desc: '',
      args: [],
    );
  }

  /// `Start`
  String get ini {
    return Intl.message(
      'Start',
      name: 'ini',
      desc: '',
      args: [],
    );
  }

  /// `End`
  String get end {
    return Intl.message(
      'End',
      name: 'end',
      desc: '',
      args: [],
    );
  }

  /// `Project`
  String get project {
    return Intl.message(
      'Project',
      name: 'project',
      desc: '',
      args: [],
    );
  }

  /// `Task`
  String get task {
    return Intl.message(
      'Task',
      name: 'task',
      desc: '',
      args: [],
    );
  }

  /// `Interval`
  String get interval {
    return Intl.message(
      'Interval',
      name: 'interval',
      desc: '',
      args: [],
    );
  }

  /// `Create task`
  String get new_task {
    return Intl.message(
      'Create task',
      name: 'new_task',
      desc: '',
      args: [],
    );
  }

  /// `Create project`
  String get new_project {
    return Intl.message(
      'Create project',
      name: 'new_project',
      desc: '',
      args: [],
    );
  }

  /// `Search by tag`
  String get search_tag {
    return Intl.message(
      'Search by tag',
      name: 'search_tag',
      desc: '',
      args: [],
    );
  }

  /// `Search by timestamps`
  String get search_interval {
    return Intl.message(
      'Search by timestamps',
      name: 'search_interval',
      desc: '',
      args: [],
    );
  }

  /// `Insert project's name`
  String get input_project_name {
    return Intl.message(
      'Insert project\'s name',
      name: 'input_project_name',
      desc: '',
      args: [],
    );
  }

  /// `Insert task's name`
  String get input_task_name {
    return Intl.message(
      'Insert task\'s name',
      name: 'input_task_name',
      desc: '',
      args: [],
    );
  }

  /// `Tarea  {activity_name} empezada`
  String task_started(Object activity_name) {
    return Intl.message(
      'Tarea  $activity_name empezada',
      name: 'task_started',
      desc: '',
      args: [activity_name],
    );
  }

  /// `Tarea  {activity_name} detenida`
  String task_stopped(Object activity_name) {
    return Intl.message(
      'Tarea  $activity_name detenida',
      name: 'task_stopped',
      desc: '',
      args: [activity_name],
    );
  }

  /// `Not started yet`
  String get not_started_project {
    return Intl.message(
      'Not started yet',
      name: 'not_started_project',
      desc: '',
      args: [],
    );
  }

  /// `Not started yet`
  String get not_started_task {
    return Intl.message(
      'Not started yet',
      name: 'not_started_task',
      desc: '',
      args: [],
    );
  }

  /// `{mesFinal}-{diaFinal}-{yearFinal} {hourFinal}:{minFinal}:{secFinal}`
  String date_format(Object mesFinal, Object diaFinal, Object yearFinal, Object hourFinal, Object minFinal, Object secFinal) {
    return Intl.message(
      '$mesFinal-$diaFinal-$yearFinal $hourFinal:$minFinal:$secFinal',
      name: 'date_format',
      desc: '',
      args: [mesFinal, diaFinal, yearFinal, hourFinal, minFinal, secFinal],
    );
  }

  /// `Search by date`
  String get search_date {
    return Intl.message(
      'Search by date',
      name: 'search_date',
      desc: '',
      args: [],
    );
  }

  /// `Initial date and hour`
  String get initial_date_time {
    return Intl.message(
      'Initial date and hour',
      name: 'initial_date_time',
      desc: '',
      args: [],
    );
  }

  /// `Final date and hour`
  String get final_date_time {
    return Intl.message(
      'Final date and hour',
      name: 'final_date_time',
      desc: '',
      args: [],
    );
  }

  /// `Date`
  String get date {
    return Intl.message(
      'Date',
      name: 'date',
      desc: '',
      args: [],
    );
  }

  /// `Hour`
  String get hour {
    return Intl.message(
      'Hour',
      name: 'hour',
      desc: '',
      args: [],
    );
  }

  /// `What tag name do you want to search for?`
  String get search_tag_ask {
    return Intl.message(
      'What tag name do you want to search for?',
      name: 'search_tag_ask',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get search {
    return Intl.message(
      'Search',
      name: 'search',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en', countryCode: 'US'),
      Locale.fromSubtags(languageCode: 'ca', countryCode: 'ES'),
      Locale.fromSubtags(languageCode: 'es', countryCode: 'ES'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}