// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es_ES locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es_ES';

  static m0(mesFinal, diaFinal, yearFinal, hourFinal, minFinal, secFinal) => "${diaFinal}-${mesFinal}-${yearFinal} ${hourFinal}:${minFinal}:${secFinal}";

  static m1(activity_name) => "Tarea  ${activity_name} empezada";

  static m2(activity_name) => "Tarea  ${activity_name} detenida";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "appTitle" : MessageLookupByLibrary.simpleMessage("Traqueador de tiempo"),
    "date" : MessageLookupByLibrary.simpleMessage("Fecha"),
    "date_format" : m0,
    "end" : MessageLookupByLibrary.simpleMessage("Fin"),
    "final_date_time" : MessageLookupByLibrary.simpleMessage("Fecha y hora final"),
    "hour" : MessageLookupByLibrary.simpleMessage("Hora"),
    "ini" : MessageLookupByLibrary.simpleMessage("Inicio"),
    "initial_date_time" : MessageLookupByLibrary.simpleMessage("Fecha y hora inicial"),
    "input_project_name" : MessageLookupByLibrary.simpleMessage("Introduce el nombre del proyecto"),
    "input_task_name" : MessageLookupByLibrary.simpleMessage("Introduce el nombre de la tarea"),
    "interval" : MessageLookupByLibrary.simpleMessage("Intervalo"),
    "new_project" : MessageLookupByLibrary.simpleMessage("Crear proyecto"),
    "new_task" : MessageLookupByLibrary.simpleMessage("Crear tarea"),
    "not_started_project" : MessageLookupByLibrary.simpleMessage("No iniciado"),
    "not_started_task" : MessageLookupByLibrary.simpleMessage("No iniciada"),
    "project" : MessageLookupByLibrary.simpleMessage("Proyecto"),
    "search" : MessageLookupByLibrary.simpleMessage("Buscar"),
    "search_date" : MessageLookupByLibrary.simpleMessage("Búsqueda por fecha"),
    "search_interval" : MessageLookupByLibrary.simpleMessage("Búsqueda por intervalos"),
    "search_tag" : MessageLookupByLibrary.simpleMessage("Búsqueda por etiqueta"),
    "search_tag_ask" : MessageLookupByLibrary.simpleMessage("¿Qué nombre de etiqueta deseas buscar?"),
    "task" : MessageLookupByLibrary.simpleMessage("Tarea"),
    "task_started" : m1,
    "task_stopped" : m2
  };
}
