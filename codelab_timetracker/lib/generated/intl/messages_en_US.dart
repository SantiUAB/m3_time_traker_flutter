// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en_US locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en_US';

  static m0(mesFinal, diaFinal, yearFinal, hourFinal, minFinal, secFinal) => "${mesFinal}-${diaFinal}-${yearFinal} ${hourFinal}:${minFinal}:${secFinal}";

  static m1(activity_name) => "Tarea  ${activity_name} empezada";

  static m2(activity_name) => "Tarea  ${activity_name} detenida";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "appTitle" : MessageLookupByLibrary.simpleMessage("Time tracker"),
    "date" : MessageLookupByLibrary.simpleMessage("Date"),
    "date_format" : m0,
    "end" : MessageLookupByLibrary.simpleMessage("End"),
    "final_date_time" : MessageLookupByLibrary.simpleMessage("Final date and hour"),
    "hour" : MessageLookupByLibrary.simpleMessage("Hour"),
    "ini" : MessageLookupByLibrary.simpleMessage("Start"),
    "initial_date_time" : MessageLookupByLibrary.simpleMessage("Initial date and hour"),
    "input_project_name" : MessageLookupByLibrary.simpleMessage("Insert project\'s name"),
    "input_task_name" : MessageLookupByLibrary.simpleMessage("Insert task\'s name"),
    "interval" : MessageLookupByLibrary.simpleMessage("Interval"),
    "new_project" : MessageLookupByLibrary.simpleMessage("Create project"),
    "new_task" : MessageLookupByLibrary.simpleMessage("Create task"),
    "not_started_project" : MessageLookupByLibrary.simpleMessage("Not started yet"),
    "not_started_task" : MessageLookupByLibrary.simpleMessage("Not started yet"),
    "project" : MessageLookupByLibrary.simpleMessage("Project"),
    "search" : MessageLookupByLibrary.simpleMessage("Search"),
    "search_date" : MessageLookupByLibrary.simpleMessage("Search by date"),
    "search_interval" : MessageLookupByLibrary.simpleMessage("Search by timestamps"),
    "search_tag" : MessageLookupByLibrary.simpleMessage("Search by tag"),
    "search_tag_ask" : MessageLookupByLibrary.simpleMessage("What tag name do you want to search for?"),
    "task" : MessageLookupByLibrary.simpleMessage("Task"),
    "task_started" : m1,
    "task_stopped" : m2
  };
}
