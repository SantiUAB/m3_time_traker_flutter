// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ca_ES locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ca_ES';

  static m0(mesFinal, diaFinal, yearFinal, hourFinal, minFinal, secFinal) => "${diaFinal}-${mesFinal}-${yearFinal} ${hourFinal}:${minFinal}:${secFinal}";

  static m1(activity_name) => "Tarea  ${activity_name} començada";

  static m2(activity_name) => "Tarea  ${activity_name} parada";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "appTitle" : MessageLookupByLibrary.simpleMessage("Aplicatiu de traqueig temporal"),
    "date" : MessageLookupByLibrary.simpleMessage("Data"),
    "date_format" : m0,
    "end" : MessageLookupByLibrary.simpleMessage("Final"),
    "final_date_time" : MessageLookupByLibrary.simpleMessage("Data i hora final"),
    "hour" : MessageLookupByLibrary.simpleMessage("Hora"),
    "ini" : MessageLookupByLibrary.simpleMessage("Inici"),
    "initial_date_time" : MessageLookupByLibrary.simpleMessage("Data i hora inicial"),
    "input_project_name" : MessageLookupByLibrary.simpleMessage("Introdueix el nom del projecte"),
    "input_task_name" : MessageLookupByLibrary.simpleMessage("Introduce el nom de la tasca"),
    "interval" : MessageLookupByLibrary.simpleMessage("Interval"),
    "new_project" : MessageLookupByLibrary.simpleMessage("Crea projecte"),
    "new_task" : MessageLookupByLibrary.simpleMessage("Crea tasca"),
    "not_started_project" : MessageLookupByLibrary.simpleMessage("No iniciat"),
    "not_started_task" : MessageLookupByLibrary.simpleMessage("No iniciada"),
    "project" : MessageLookupByLibrary.simpleMessage("Projecte"),
    "search" : MessageLookupByLibrary.simpleMessage("Cerca"),
    "search_date" : MessageLookupByLibrary.simpleMessage("Cerca per data"),
    "search_interval" : MessageLookupByLibrary.simpleMessage("Cerca per intervals"),
    "search_tag" : MessageLookupByLibrary.simpleMessage("Cerca per etiqueta"),
    "search_tag_ask" : MessageLookupByLibrary.simpleMessage("¿Per quin nom d\'etiqueta vols buscar?"),
    "task" : MessageLookupByLibrary.simpleMessage("Tasca"),
    "task_started" : m1,
    "task_stopped" : m2
  };
}
