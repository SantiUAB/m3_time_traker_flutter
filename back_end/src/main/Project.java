package main;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*Esta clase forma parte del patrón composite implementado, utiliza variables de la clase
* Activity y también el método readInfo(...)los proyectos pueden tener tareas derivadas,
* y también es importante contemplar que los proyectos pueden tener proyectos derivados.
 */
public class Project extends Activity {
  static Logger loggerM1 = LoggerFactory.getLogger("timetracker.m1." + Task.class.getName());
  static Logger loggerM2 = LoggerFactory.getLogger("timetracker.m2." + Task.class.getName());

  private ArrayList<Activity> activityList = new ArrayList<>();

  private Clock clock;

  private boolean invariant() {
    loggerM2.info("Initialising invariant conditions");
    boolean correct = false;
    if (this.getName() != null && getActivityList() != null) { //TODO que mas se pude contemplar?
      correct = true;
    }
    return correct;
  }

  public Project(String name) {
    super(name);
    loggerM1.trace("Entering on constructor Project(String name) done");
  }

  public Project(String name, long id) {
    super(name, id);
    loggerM1.trace("Entering on constructor Project(String name) done");
  }

  public Project(String name, long id, Clock clock) {
    super(name, id);
    this.clock = clock;
    loggerM1.trace("Entering on constructor Project(String name) done");
  }

  public Project(String value, Activity father, LocalDateTime initialDateTime,
                 LocalDateTime finalDateTime, Duration duration, ArrayList<Activity> activityList) {
    super(value, father, initialDateTime, finalDateTime, duration);
    loggerM1.trace("Constructor with enough arguments to add a new activityList on Project");
    this.activityList = activityList;
  }

  public Project(String value, Activity father, LocalDateTime initialDateTime,
                 LocalDateTime finalDateTime, Duration duration) {
    super(value, father, initialDateTime, finalDateTime, duration);
    loggerM1.trace("Constructor with enough arguments to create a new Super variable on Project");
  }

  public ArrayList<Activity> getActivityList() {
    loggerM1.trace("Getting activityList on Project");
    return activityList;
  }

  public void setActivityList(ArrayList<Activity> activityList) {
    loggerM1.trace("Setting activityList on Project");
    this.activityList = activityList;
  }

  public Clock getClock() {
    return clock;
  }

  private boolean assertHelper(boolean condition, String warningMsg, String correctMsg) {

    if (condition) {
      loggerM2.info(correctMsg);
    } else {
      loggerM2.warn(warningMsg);
    }

    return condition;
  }

  /*Este método añade una tarea a la lista de actividades cuándo
  * ya existe alguna instancia Task creada */

  public void addActivity(Activity testActivity) {
    assert assertHelper(invariant(), "Invariant condition has failed",
        "Invariant condition went as expected");

    //pre-conditions
    assert assertHelper(testActivity != null, "Activity empty", "Activity added well");

    int countPre = getActivityList().size();

    //function
    activityList.add(testActivity);
    testActivity.setParent(this);

    int contPos = activityList.size();
    // post-conditions
    assert assertHelper(contPos == countPre + 1, "Position counter not working",
        "Position counter works properly");
    assert assertHelper(testActivity.getParent() != null, "Parent of the activity is null",
        "Parent of the activity is not null");
    assert assertHelper(invariant(), "Invariant condition has failed",
        "Invariant condition went as expected");
  }

  /*Implementación de la función ubicada en la clase Activity como abstracta,
    se controla cuál será el padre del proyecto para así tener conocimiento de la jerarquía
    de actividades y se printa por pantalla la información obtenida.
    Se implementa en esta clase debido a que es la que conoce a todos los proyectos derivados
    al igual que las tareas */
  public void readInfo(LocalDateTime initialTaskTime, LocalDateTime finalTaskTime, long seconds) {

    assert assertHelper(invariant(), "Invariant condition has failed",
        "Invariant condition went as expected");
    // pre-conditions
    if (this.getParent() != null) {
      assert assertHelper(initialTaskTime != null, "Initial Project task time is null",
          "Initial Project task time not null");
    }

    assert assertHelper(seconds >= 0, "Time invalid value", "Timing working properly");


    if (this.getInitialDate() == null) {
      this.setInitialDate(initialTaskTime);
    }

    if (this.getParent() != null) {
      this.getParent().readInfo(this.getInitialDate(), this.getFinalDate(), seconds);
    }

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    if (finalTaskTime != null) {
      this.setFinalDate(finalTaskTime);
    }

    this.setDuration(this.getDuration().plus(Duration.ofSeconds(seconds)));

    if (getParent() != null) {

      String initialTemp;
      String endTemp;

      if (getInitialDate() == null) {
        initialTemp = "null";
      } else {
        initialTemp = this.getInitialDate().format(formatter);
      }

      if (getFinalDate() == null) {
        endTemp = "null";
      } else {
        endTemp = this.getFinalDate().format(formatter);
      }


      System.out.format("%-16s %-19s %-25s %-25s %-10s\n", this.getName(),
          this.getParent().getName(), initialTemp, endTemp,
          this.getDuration().toSecondsPart());
    } else {

      String initialTemp;
      String endTemp;

      if (getInitialDate() == null) {
        initialTemp = "null";
      } else {
        initialTemp = this.getInitialDate().format(formatter);
      }

      if (getFinalDate() == null) {
        endTemp = "null";
      } else {
        endTemp = this.getFinalDate().format(formatter);
      }

      System.out.format("%-16s %-19s %-25s %-25s %-10s\n", this.getName(), "null", initialTemp,
          endTemp, this.getDuration().toSecondsPart());
    }

    // post-conditions
    assert assertHelper(this.getDuration() != null, "Duration of the Project is null",
        "Duration of the Project not null");
    assert assertHelper(invariant(), "Invariant condition has failed",
        "Invariant condition went as expected");
  }

  @Override
   public void accept(Visitor visitor, LocalDateTime ini, LocalDateTime end) {
    visitor.searchTimeSpend(this,  ini, end);
  }

  @Override
  public void accept(Visitor visitor, String tag) {
    visitor.searchTag(this, tag);
    loggerM2.info("Accepting visitor");
  }

  @Override
  public void accept(Visitor visitor, long id) {
    visitor.findActivityById(this, id);
    loggerM2.info("Accepting visitor");
  }

}
