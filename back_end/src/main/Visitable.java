package main;

import java.time.LocalDateTime;

public interface Visitable {

  void accept(Visitor visitor, LocalDateTime ini, LocalDateTime end);

  void accept(Visitor visitor, String tag);

  void accept(Visitor visitor, long id);


}
