package main;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Observable;
import java.util.Observer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*La clase Interval se implementa como el observer del patrón OBSERVER,
  esta clase será la que mediante la relación con la clase Clock
  actualizará la hora a tratar. También contendrá las función necesaria
  para actualizar los intervalos */

public class Interval implements Observer {

  private final LocalDateTime initialDateTime;
  private LocalDateTime currentDataTime;
  private LocalDateTime lastDataTime;
  private Task parent;
  private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  static Logger logger = LoggerFactory.getLogger("timetracker.m1." + Interval.class.getName());

  public Interval(Task parent, Clock clock) {

    logger.trace("Entering constructor Interval(Task parent, Clock clock)");

    this.initialDateTime = clock.getCurrentDateTime();
    this.currentDataTime = clock.getCurrentDateTime();
    this.lastDataTime = clock.getCurrentDateTime();
    this.parent = parent;

    logger.info("Interval created for task {}", parent.getName());

    logger.debug("Interval parent: {}", parent.getName());
    logger.debug("Interval initial date: {}", initialDateTime);
    logger.debug("Interval final date: {}", lastDataTime);

  }

  public Interval(LocalDateTime ini, LocalDateTime fin, Activity father) {

    logger.trace("Entering constructor Interval(LocalDateTime ini, LocalDateTime fin, "
        + "Activity father)");

    this.initialDateTime = ini;
    this.lastDataTime = fin;
    this.parent = (Task) father;

    logger.info("Interval created for task {}", parent.getName());

    logger.debug("Interval parent: {}", parent.getName());
    logger.debug("Interval initial date: {}", initialDateTime);
    logger.debug("Interval final date: {}", lastDataTime);

  }

  public Interval(LocalDateTime ini, LocalDateTime fin) {
    logger.trace("Entering constructor Interval(LocalDateTime ini, LocalDateTime fin)");

    this.initialDateTime = ini;
    this.lastDataTime = fin;

    logger.debug("Interval parent: {}", parent.getName());
    logger.debug("Interval initial date: {}", initialDateTime);
    logger.debug("Interval final date: {}", lastDataTime);

  }

  public LocalDateTime getInitialDateTime() {
    return initialDateTime;
  }

  public LocalDateTime getCurrentDataTime() {
    return currentDataTime;
  }

  public LocalDateTime getLastDataTime() {
    return lastDataTime;
  }

  public Task getParent() {
    return parent;
  }


  /*Este método es el que actualizará la fecha y hora cuando convenga.
    Si todavía no hay ninguna fecha y hora asignada a una actividad
    se la inicializará correspondientemente */
  @Override
  public void update(Observable o, Object arg) {

    logger.trace("Updating time through an Observable");

    this.lastDataTime = currentDataTime;
    this.currentDataTime = (LocalDateTime) arg;

    long seconds = lastDataTime.until(currentDataTime, ChronoUnit.SECONDS);

    System.out.format("%-16s %-19s %-25s %-25s %-10s\n", "Interval", this.getParent().getName(),
        this.getInitialDateTime().format(formatter), this.getCurrentDataTime().format(formatter),
        Duration.between(getInitialDateTime(), getLastDataTime()).toSecondsPart());
    parent.readInfo(initialDateTime, currentDataTime, seconds);

  }

}
