package main;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Field;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;


//Clase que se encarga de leer/escribir en un archivo .json
// todas las actividades de un Activity root de forma recursiva.
public class JsonManagerRecursive {

  public static final String SAVE_TO = "./data/";
  private static final String NAME = "name";
  private static final String ID = "id";
  private static final String CLASS = "class";
  private static final String TASK = "task";
  private static final String PROJECT = "project";
  private static final String INITIAL_DATE = "initialDate";
  private static final String FINAL_DATE = "finalDate";
  private static final String DURATION = "duration";
  private static final String ACTIVITIES = "activities";
  private static final String INTERVALS = "intervals";
  private static final String TAGS = "tags";
  private static final String TAG = "tag";
  private static final String FORMAT_DATE = "yyyy-MM-dd HH:mm:ss";

  private String fileName;
  private Activity rootActivity;
  private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_DATE);
  private int depthCount;

  static Logger loggerM1 = LoggerFactory.getLogger("timetracker.m1."
      + JsonManagerRecursive.class.getName());
  static Logger loggerM2 = LoggerFactory.getLogger("timetracker.m2."
      + JsonManagerRecursive.class.getName());

  public void writeToFile(String fileName, Activity rootActivity) throws IOException {

    loggerM1.info("Starting writeToFile for Activity {} to file {}",
        rootActivity.getName(), fileName);

    this.fileName = fileName;
    this.rootActivity = rootActivity;

    JSONObject jsonObject = new JSONObject();
    forceToOrderJson(jsonObject);
    createRootProjectJson(jsonObject);

    ArrayList<Activity> activitiesToSave = ((Project) rootActivity).getActivityList();

    jsonObject.put(ACTIVITIES, recursiveChilds(activitiesToSave, jsonObject));

    writeDown(fileName, jsonObject);

  }

  public JSONObject ActivityToJson(Activity rootActivity, int depth) {

    loggerM1.info("Starting ActivityToJson for Activity {}",
        rootActivity.getName());
    this.depthCount = depth;


    this.rootActivity = rootActivity;

    loggerM1.info("root activity {}",
            rootActivity.getName());

    JSONObject jsonObject = new JSONObject();
    forceToOrderJson(jsonObject);

    loggerM1.info("depth ----------{}" , this.depthCount);

    loggerM1.info("depth -------1 ---{}" , this.depthCount);
    try {
      if (rootActivity instanceof Task) {
        createTaskJson(rootActivity, jsonObject);
      } else {
        createRootProjectJson(jsonObject);
      }
    }catch (Exception e)
    {
     loggerM1.info("solo el de arriba sabe porque funciona esto, pero es necesario este try-catch");
      loggerM1.info( "error {}", e.getMessage() );
    }
    loggerM1.info("depth -------2 ---{}" , this.depthCount);


    if (this.depthCount <= 0) {
      return jsonObject;
    }



    if (rootActivity instanceof Project) {

      ArrayList<Activity> activitiesToSave = ((Project) rootActivity).getActivityList();

      jsonObject.put(ACTIVITIES, recursiveChildsCount(activitiesToSave, jsonObject));
    }


    return jsonObject;

  }


  //Método que recorre la ActivityList del root y crea a sus hijos y propiedades en base
  // a si son de la clase Project o Task de forma recursiva
  private JSONArray recursiveChilds(ArrayList<Activity> activitiesToSave,
                                    JSONObject jsonInsideObject) {

    loggerM1.trace("Entering recursiveChilds() (recursive)");

    JSONArray jsonArray = new JSONArray();

    for (Activity activityInside : activitiesToSave) {

      JSONObject tempJson = new JSONObject();
      forceToOrderJson(tempJson);
      if (activityInside instanceof Project) {
        createProjectJson(activityInside, tempJson);
      }

      if (activityInside instanceof Task) {
        createTaskJson(activityInside, tempJson);
      }

      jsonArray.put(tempJson);

    }

    return jsonArray;

  }

  //Método que recorre la ActivityList del root y crea a sus hijos y propiedades en base
  // a si son de la clase Project o Task de forma recursiva
  private JSONArray recursiveChildsCount(ArrayList<Activity> activitiesToSave,
                                    JSONObject jsonInsideObject) {

    loggerM1.trace("Entering recursiveChildsCount() (recursive)");
    this.depthCount--;

    JSONArray jsonArray = new JSONArray();

    if (this.depthCount <= 0) {

      return jsonArray;

    } else {

      for (Activity activityInside : activitiesToSave) {

        JSONObject tempJson = new JSONObject();
        forceToOrderJson(tempJson);

        if (activityInside instanceof Project) {
          createProjectJsonCount(activityInside, tempJson);
        }

        if (activityInside instanceof Task) {
          createTaskJson(activityInside, tempJson);
        }

        jsonArray.put(tempJson);

      }

    }

    return jsonArray;

  }


  //Método que hace que la salida del archivo .json se haga en el orden
  // en el que se guardan los atributos de cada Activity
  private void forceToOrderJson(JSONObject jsonObject) {

    loggerM1.trace("Sorting JSON Object");

    try {
      Field changeMap = jsonObject.getClass().getDeclaredField("map");
      changeMap.setAccessible(true);
      changeMap.set(jsonObject, new LinkedHashMap<>());
      changeMap.setAccessible(false);
    } catch (IllegalAccessException | NoSuchFieldException e) {
      loggerM1.error(e.getMessage());
      e.printStackTrace();
    }
  }


  private void createRootProjectJson(JSONObject jsonObject) {

    loggerM1.trace("Creating Root project JSON");

    jsonObject.put(NAME, rootActivity.getName());
    jsonObject.put("id", rootActivity.getId());
    jsonObject.put(CLASS, PROJECT);

    jsonObject.put(INITIAL_DATE, rootActivity.getInitialDate().format(formatter));
    jsonObject.put(FINAL_DATE, rootActivity.getFinalDate().format(formatter));
    jsonObject.put(DURATION, rootActivity.getDuration().toSecondsPart());
  }

  private void createProjectJson(Activity activityInside, JSONObject jsonInsideObject) {

    loggerM1.trace("Creating JSON from a project");

    jsonInsideObject.put(NAME, activityInside.getName());
    jsonInsideObject.put(CLASS, PROJECT);

    if (activityInside.getInitialDate() != null) {
      jsonInsideObject.put(INITIAL_DATE, activityInside.getInitialDate().format(formatter));
    } else {
      jsonInsideObject.put(INITIAL_DATE, JSONObject.NULL);
    }
    if (activityInside.getFinalDate() != null) {
      jsonInsideObject.put(FINAL_DATE, activityInside.getFinalDate().format(formatter));
    } else {
      jsonInsideObject.put(FINAL_DATE, JSONObject.NULL);
    }
    jsonInsideObject.put(TAGS, createTagsJson(activityInside.getTagList()));

    jsonInsideObject.put(ACTIVITIES, recursiveChilds(((Project) activityInside).getActivityList(),
        jsonInsideObject));
  }

  private void createProjectJsonCount(Activity activityInside, JSONObject jsonInsideObject) {

    loggerM1.trace("Creating JSON from a project");

    jsonInsideObject.put(NAME, activityInside.getName());
    jsonInsideObject.put(CLASS, PROJECT);

    jsonInsideObject.put(ID, activityInside.getId());

    if (activityInside.getInitialDate() != null) {
      jsonInsideObject.put(INITIAL_DATE, activityInside.getInitialDate().format(formatter));
    } else {
      jsonInsideObject.put(INITIAL_DATE, JSONObject.NULL);
    }
    if (activityInside.getFinalDate() != null) {
      jsonInsideObject.put(FINAL_DATE, activityInside.getFinalDate().format(formatter));
    } else {
      jsonInsideObject.put(FINAL_DATE, JSONObject.NULL);
    }

    if (activityInside.getDuration() != null) {
      jsonInsideObject.put(DURATION, activityInside.getDuration().toSeconds());
    } else {
      jsonInsideObject.put(DURATION, JSONObject.NULL);
    }


    jsonInsideObject.put(TAGS, createTagsJson(activityInside.getTagList()));

    jsonInsideObject.put(ACTIVITIES, recursiveChildsCount(((Project) activityInside).getActivityList(),
        jsonInsideObject));
  }

  private void createTaskJson(Activity activityInside, JSONObject jsonInsideObject) {

    loggerM1.trace("Creating JSON from a task");

    jsonInsideObject.put(NAME, activityInside.getName());
    jsonInsideObject.put(CLASS, TASK);

    jsonInsideObject.put(ID, activityInside.getId());

    jsonInsideObject.put("active", ((Task) activityInside).isActive());
    // TODO leer atributo active
    if (activityInside.getInitialDate() != null) {
      jsonInsideObject.put(INITIAL_DATE, activityInside.getInitialDate().format(formatter));
    } else {
      jsonInsideObject.put(INITIAL_DATE, JSONObject.NULL);
    }
    if (activityInside.getFinalDate() != null) {
      jsonInsideObject.put(FINAL_DATE, activityInside.getFinalDate().format(formatter));
    } else {
      jsonInsideObject.put(FINAL_DATE, JSONObject.NULL);
    }
    jsonInsideObject.put(TAGS, createTagsJson(activityInside.getTagList()));
    jsonInsideObject.put(DURATION, activityInside.getDuration().toSeconds());
    jsonInsideObject.put(INTERVALS, createIntervalJson(((Task) activityInside).getIntervalList()));
  }

  private JSONArray createIntervalJson(ArrayList<Interval> list) {

    loggerM1.trace("Creating JSON structure from an Interval list");

    JSONArray tempArray = new JSONArray();

    for (Interval interval : list) {

      JSONObject tempObject = new JSONObject();
      forceToOrderJson(tempObject);

      tempObject.put(INITIAL_DATE, interval.getInitialDateTime().format(formatter));
      tempObject.put(FINAL_DATE, interval.getLastDataTime().format(formatter));

      tempArray.put(tempObject);

    }

    return tempArray;
  }

  private JSONArray createTagsJson(ArrayList<String> list) {

    JSONArray tempArray = new JSONArray();
    loggerM1.trace("Creating JSON structure from a Tag list");

    for (String tag : list) {

      JSONObject tempObject = new JSONObject();
      forceToOrderJson(tempObject);

      tempObject.put(TAG, tag);
      //tempObject.put(FINAL_DATE, interval.getLastDataTime().format(formatter));

      tempArray.put(tempObject);

    }

    return tempArray;
  }

  private void writeDown(String fileName, JSONObject jsonObject) throws IOException {

    loggerM1.trace("Entering writeDown() ({})", fileName);

    File file = new File(fileName);

    try (FileWriter writer = new FileWriter(SAVE_TO + file)) {
      jsonObject.write(writer);
      System.out.println("El archivo ha sido correctamente creado en " + SAVE_TO + file);
      loggerM1.info("El archivo ha sido correctamente creado en " + SAVE_TO + file);
    } catch (FileNotFoundException e) {
      loggerM1.error(e.getMessage());
      e.printStackTrace();
    }
  }

  /* Se encarga de leer el fichero JSON de la ubicación filename, esta función devuelve
  un activity root con toda la información contenida en el fichero JSON*/
  public Activity readFromFile(String filename) {

    loggerM1.trace("Entering readFromFile() ({})", fileName);

    this.fileName = filename;
    JSONObject object;
    JSONTokener tokener;

    File targetFile = new File(this.fileName);

    try {
      InputStream is = new FileInputStream(targetFile);

      if (is == null) {
        loggerM1.error("Cannot find resource file {}", this.fileName);
        throw  new NullPointerException("Cannot find resource file " + this.fileName);
      }

      tokener = new JSONTokener(is);
      object  = new JSONObject(tokener);

      // TODO toString main.Activity
      System.out.println("Name :  " + object.get(NAME));
      System.out.println("ID :  " + getValue(object, ID));
      System.out.println("Class :  " + object.get(CLASS));
      System.out.println("Initial Date : " + object.get(INITIAL_DATE));
      System.out.println("Final Date : " + object.get(FINAL_DATE));
      System.out.println("Duration : " + object.get(DURATION));

      System.out.println("activities - Root: ");

      JSONArray jsonArrayActivity =  getArray(object, ACTIVITIES);
      ArrayList<Activity> activityList = new ArrayList<Activity>();

      this.rootActivity = new Project(getValue(object, NAME),
              null,
              getValueDate(object, INITIAL_DATE),
              getValueDate(object, FINAL_DATE),
              getDurarion(object, DURATION));

      for (int i = 0; i < jsonArrayActivity.length(); i++) {
        Activity activity = createActivity((JSONObject) jsonArrayActivity.get(i),
                this.rootActivity);
        activityList.add(activity);
      }

      ((Project) this.rootActivity).setActivityList(activityList);

    } catch (FileNotFoundException e) {
      loggerM1.error(e.getMessage());
      e.printStackTrace();
    }

    return rootActivity;
  }

  /*Función recursiva que se encarga de leer las actividades de segundo nivel,
  estas pueden ser proyectos o task. Por lo tanto, devuelve una Activity segundo nivel.*/
  private Activity createActivity(JSONObject obj, Activity activityFather) {

    loggerM1.trace("Entering createActivity (recursive)");

    // TODO toString main.Activity
    System.out.println("\t" + "Name :  " + getValue(obj, NAME));
    System.out.println("\t" + "Class :  " + getValue(obj, CLASS));
    System.out.println("\t" + "Initial Date :  " + getValue(obj, INITIAL_DATE));
    System.out.println("\t" + "Final Date :  " + getValue(obj, FINAL_DATE));
    System.out.println("\t" + "Duration :  " + getValue(obj, DURATION));

    loggerM1.debug("\t" + "Name :  " + getValue(obj, NAME));
    loggerM1.debug("\t" + "Class :  " + getValue(obj, CLASS));
    loggerM1.debug("\t" + "Initial Date :  " + getValue(obj, INITIAL_DATE));
    loggerM1.debug("\t" + "Final Date :  " + getValue(obj, FINAL_DATE));
    loggerM1.debug("\t" + "Duration :  " + getValue(obj, DURATION));


    Activity activity = null;

    if (getValue(obj, CLASS).equals(TASK)) {

      System.out.println("\t" + "intervals :");
      JSONArray courserInterval = getArray(obj, INTERVALS);

      ArrayList<Interval> intervalList = new ArrayList<>();
      activity = new Task(getValue(obj, NAME),
              (Project) activityFather,
              getValueDate(obj, INITIAL_DATE),
              getValueDate(obj, FINAL_DATE),
              getDurarion(obj, DURATION)
      );
      for (int j = 0; j < courserInterval.length(); j++) {
        Interval interval = createInterval((JSONObject) courserInterval.get(j), activity);
        intervalList.add(interval);
      }

      System.out.println("\t" + "tags :");
      JSONArray courserTags = getArray(obj, TAGS);
      ArrayList<String> tagsList = new ArrayList<>();
      for (int j = 0; j < courserTags.length(); j++) {
        //Interval interval = createInterval((JSONObject) courserInterval.get(j), activity);
        String tag = createTag((JSONObject) courserTags.get(j));
        tagsList.add(tag);
      }

      activity.setTagList(tagsList);
      ((Task) activity).setIntervaList(intervalList);

    } else if (getValue(obj, CLASS).equals(PROJECT)) {
      JSONArray courserActivity = getArray(obj, ACTIVITIES);

      ArrayList<Activity> activityList = new ArrayList<>();
      for (int j = 0; j < courserActivity.length(); j++) {
        Activity recursiveActivity = createActivity(
                (JSONObject) courserActivity.get(j),
                activityFather);
        activityList.add(recursiveActivity);
      }

      System.out.println("\t" + "tags :");
      JSONArray courserTags = getArray(obj, TAGS);
      ArrayList<String> tagsList = new ArrayList<>();
      for (int j = 0; j < courserTags.length(); j++) {
        //Interval interval = createInterval((JSONObject) courserInterval.get(j), activity);
        String tag = createTag((JSONObject) courserTags.get(j));
        tagsList.add(tag);
      }



      activity = new Project(getValue(obj, NAME),
              activityFather,
              getValueDate(obj, INITIAL_DATE),
              getValueDate(obj, FINAL_DATE),
              getDurarion(obj, DURATION),
              activityList);
      activity.setTagList(tagsList);
    }

    return activity;
  }

  private String createTag(JSONObject obj) {
    System.out.println(" \t\t " + " tag : " + getValue(obj, TAG));
    return getValue(obj, TAG);
  }

  private Interval createInterval(JSONObject obj, Activity father) {

    loggerM1.trace("Entering createInterval()");

    System.out.println(" \t\t " + " START " + getValue(obj, INITIAL_DATE));
    System.out.println(" \t\t " + " END " + getValue(obj, FINAL_DATE));
    LocalDateTime ini = getValueDate(obj, INITIAL_DATE);
    LocalDateTime fin = getValueDate(obj, FINAL_DATE);
    return new Interval(ini, fin, father);
  }

  private JSONArray getArray(JSONObject objArray, String key) {

    loggerM1.trace("Getting JSONArray");

    if (objArray.isNull(key)) {
      return new JSONArray();
    }
    return objArray.getJSONArray(key);
  }


  private String getValue(JSONObject obj, String key) {

    loggerM1.trace("Getting value from {}", key);

    if (obj.isNull(key)) {
      return "null";
    }
    String  txt = String.valueOf(obj.get(key));
    return txt;
  }

  /*Transforma un valor String a LocalDateTime*/
  private LocalDateTime getValueDate(JSONObject obj, String key) {

    loggerM1.trace("Getting date value");

    LocalDateTime localDateTime = null;

    if (obj.isNull(key)) {
      return localDateTime;
    } else if (getValue(obj, key).equals("null")) {
      return  localDateTime;
    } else if (key ==  INITIAL_DATE || key == FINAL_DATE) {
      String initalDate = getValue(obj, key);
      DateTimeFormatter format = DateTimeFormatter.ofPattern(FORMAT_DATE);
      localDateTime = LocalDateTime.parse(initalDate, format);
    }
    return localDateTime;
  }

  /*Transforma un valor String a Duration*/
  private Duration getDurarion(JSONObject obj, String key) {

    loggerM1.trace("Getting duration");

    Duration duration = null;

    if (obj.isNull(key)) {
      return duration;
    } else if (key.equals(DURATION)) {
      String strDuration = getValue(obj, key);
      long longDuration = Long.parseLong(strDuration);
      duration = Duration.ofSeconds(longDuration);
    }
    return duration;
  }
}
