package main;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

  static Logger logger = LoggerFactory.getLogger("timetracker.m1." + Main.class.getName());
  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  public static void main(String[] args) throws IOException {

    LocalDateTime iniTime = LocalDateTime.now();

    logger.info("TEST CREACION ARBOL");
    Project proot = testTreeProject();

    JsonManagerRecursive recursive = new JsonManagerRecursive();
    logger.info("\nTEST ESCRITURA EN JSON");
    recursive.writeToFile("activities.json", proot);

    logger.info("\nTEST LECTURA EN JSON");
    Activity activityRoot = recursive.readFromFile(JsonManagerRecursive.SAVE_TO
        + "activities.json");


    System.out.println("Vamos a contar. Tiempo total = " + testAppendixC());

    System.exit(0);

  }

  private static Project testTreeProject() {
    //String hour = "2020-09-22 16:04:56";
    Clock clock = new Clock();
    clock.startClock(2);

    Project proot = new Project("ROOT");

    //HIJOS DE ROOT
    Project psoftDesign = new Project("Software Design");
    Project psoftTesting = new Project("Software Testing");
    Project pdataBase = new Project("Databases");
    Task ttransport = new Task("transportation", clock);

    proot.addActivity(psoftDesign);
    proot.addActivity(psoftTesting);
    proot.addActivity(pdataBase);
    proot.addActivity(ttransport);

    //HIJOS DE SOFTWARE DESIGN
    Project pproblems = new Project("Problems");
    Project ptimeTracker = new Project("Time Tracker");

    psoftDesign.addActivity(pproblems);
    psoftDesign.addActivity(ptimeTracker);


    //HIJOS DE PROBLEMS
    Task tfirstList = new Task("First List", clock);
    Task tsecondList = new Task("Second List", clock);

    pproblems.addActivity(tfirstList);
    pproblems.addActivity(tsecondList);

    //HIJOS DE TIME TRACKER
    Task treadHandout = new Task("Read handout", clock);
    Task tfirstMilestone = new Task("First Milestone", clock);

    ptimeTracker.addActivity(treadHandout);
    ptimeTracker.addActivity(tfirstMilestone);


    //TAGS
    //Software Design
    psoftDesign.addTag("java");
    psoftDesign.addTag("flutter");
    //Software testing
    psoftTesting.addTag("c++");
    psoftTesting.addTag("Java");
    psoftTesting.addTag("python");
    //Databases
    pdataBase.addTag("SQL");
    pdataBase.addTag("python");
    pdataBase.addTag("C++");
    //Databases
    pdataBase.addTag("SQL");
    pdataBase.addTag("python");
    pdataBase.addTag("C++");
    //First list
    tfirstList.addTag("java");
    //Second list
    tsecondList.addTag("Dart");
    //First list
    tfirstMilestone.addTag("Java");
    tfirstMilestone.addTag("IntelliJ");


    Search search = new Search();

    String searchedTag = "java";

    logger.info("Classes with searched tag ({})", searchedTag);
    for (Activity activity : search.searchTag(proot, searchedTag)) {
      System.out.println(activity.getName());
    }

    //COMIENZA LA EJECUCIÓN

    logger.info("start test");
    logger.info("transportation starts");

    //1
    ttransport.start();
    wait(6);
    ttransport.stop();
    logger.info("transportation stops");

    //2
    wait(2);

    //3
    tfirstList.start();
    logger.info("first list starts");
    wait(6);

    //4
    tsecondList.start();
    logger.info("second list starts");
    wait(4);

    //5
    logger.info("first list stops");
    tfirstList.stop();

    //6
    wait(2);
    logger.info("seconds list stops");
    tsecondList.stop();

    //7
    wait(2);

    //8
    ttransport.start();
    logger.info("transportation starts");
    wait(4);
    logger.info("transportation stops");
    ttransport.stop();

    logger.info("end of test");
    return proot;
  }

  private static void wait(int seconds) {
    try {
      Thread.sleep(seconds * 1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private static int testAppendixC() {

    Clock clock = new Clock();
    clock.startClock(2);

    Project p0 = new Project("P0");
    Task t0 = new Task("T0", clock);
    Task t1 = new Task("T1", clock);
    Task t2 = new Task("T2", clock);
    p0.addActivity(t0);
    p0.addActivity(t1);
    p0.addActivity(t2);

    Project p1 = new Project("P1");
    Task t3 = new Task("T3", clock);
    p1.addActivity(t3);

    Project p3 = new Project("P3");

    Project root = new Project("root");
    Task t4 = new Task("T4", clock);
    Task t5 = new Task("T5", clock);
    root.addActivity(p0);
    root.addActivity(p1);
    root.addActivity(t4);
    root.addActivity(t5);
    root.addActivity(p3);

    System.out.println("Apendice C empieza");
    //T = 0
    wait(10);
    //T = 10
    t0.start();
    wait(10);
    //T = 20
    t4.start();
    wait(10);
    //T = 30
    t0.stop();
    t1.start();
    t2.start();
    t4.stop();
    wait(10);
    //T = 40
    t0.start();
    t5.start();
    wait(10);
    //T = 50
    t0.stop();
    t1.stop();
    t4.start();
    wait(10);
    // T = 60
    LocalDateTime iniTime = LocalDateTime.now();
    wait(10);
    //T = 70
    t1.start();
    t5.stop();
    wait(10);
    //T = 80
    t5.start();
    wait(10);
    //T = 90
    t2.stop();
    t5.stop();
    wait(10);
    //T = 100
    t5.start();
    wait(10);
    //T = 110
    t2.start();
    wait(10);
    //T = 120
    LocalDateTime endTime = LocalDateTime.now();
    wait(10);
    //T = 130
    t1.stop();
    t2.stop();
    t3.start();
    t4.stop();
    wait(10);
    //T = 140
    t0.start();
    t3.stop();
    t4.start();
    wait(10);
    //T = 150
    t0.stop();
    wait(10);
    //T = 160
    t4.stop();


    Search search = new Search();
    return search.searchTimeSpend(root, iniTime, endTime);

  }

}
