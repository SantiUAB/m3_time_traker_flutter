package main;

import java.time.LocalDateTime;
import java.util.ArrayList;

public interface Visitor {

  int searchTimeSpend(Project project, LocalDateTime iniTime, LocalDateTime endTime);

  int searchTimeSpend(Task task, LocalDateTime iniTime, LocalDateTime endTime);

  ArrayList<Activity> searchTag(Task task, String tag);

  ArrayList<Activity> searchTag(Project project, String tag);

  Activity findActivityById(Activity main, long id);
}
