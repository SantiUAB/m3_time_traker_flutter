package main;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Search implements Visitor {
  private LocalDateTime iniTime;
  private LocalDateTime endTime;
  private int seg = 0;

  static Logger loggerM2 = LoggerFactory.getLogger("timetracker.m2."
      + JsonManagerRecursive.class.getName());

  static Logger loggerM3 = LoggerFactory.getLogger("timetracker.m3."
          + JsonManagerRecursive.class.getName());

  @Override
  public int searchTimeSpend(Project project, LocalDateTime iniTime, LocalDateTime endTime) {
    for (Activity act : project.getActivityList()) {
      if (act instanceof Task) {
        searchTimeSpend((Task) act, iniTime, endTime);
      } else {
        searchTimeSpend((Project) act, iniTime, endTime);
      }
    }
    return this.seg;
  }

  @Override
  public int searchTimeSpend(Task task, LocalDateTime iniTime, LocalDateTime endTime) {
    for (Interval interval : task.getIntervalList()) {
      LocalDateTime intervalIni = interval.getInitialDateTime();
      LocalDateTime intervalEnd = interval.getLastDataTime();
      if (intervalIni.isAfter(iniTime)) {
        loggerM2.info("add intervals " + task.getName());
        loggerM2.info("\tini " + interval.getInitialDateTime());
        loggerM2.info("\tend " + interval.getLastDataTime());
        loggerM2.info("\tBetween " + Duration.between(interval.getInitialDateTime(),
            interval.getLastDataTime()).toSecondsPart());
        this.seg += Duration.between(interval.getInitialDateTime(),
            interval.getLastDataTime()).toSecondsPart();
      }
    }
    return this.seg;
  }

  @Override
  public ArrayList<Activity> searchTag(Task task, String tag) {
    ArrayList<Activity> taggedActivities = new ArrayList<>();

    /*Por cada tag en la lista de tags de la tarea, compruebo si existe
    y si la tarea no está en la lista, la añado*/

    for (String tagInTask : task.getTagList()) {
      if (tag.toLowerCase().equals(tagInTask.toLowerCase()) && !taggedActivities.contains(task)) {
        taggedActivities.add(task);
      }
    }

    return taggedActivities;
  }

  @Override
    public ArrayList<Activity> searchTag(Project project, String tag) {
    ArrayList<Activity> taggedActivities = new ArrayList<>();

    /*Por cada tag en la lista de tags del proyecto,
    compruebo si existe y si el proyecto no está en la lista,
    lo añade*/

    for (String tagInProject : project.getTagList()) {
      if (tag.toLowerCase().equals(tagInProject.toLowerCase())
          && !taggedActivities.contains(project)) {
        taggedActivities.add(project);
      }
    }

    for (Activity activityInside : project.getActivityList()) {
      if (activityInside instanceof Task) {
        taggedActivities.addAll(searchTag((Task) activityInside, tag));
      } else {
        taggedActivities.addAll(searchTag((Project) activityInside, tag));
      }
    }
    return taggedActivities;
  }

  @Override
  public Activity findActivityById(Activity main, long id) {

    Activity resultActivity = null;

    /*Por cada tag en la lista de tags del proyecto,
    compruebo si existe y si el proyecto no está en la lista,
    lo añade*/

    loggerM3.info("name {}" ,  main.getName());

    if (main.getId() == id) {
      return main;
    }

    if (main instanceof Project) {

      Project aux = (Project) main;

      loggerM3.info("Project  id {}\n" , aux.getId() );
      ArrayList<Activity> childs = aux.getActivityList();

      boolean found = false;

      for (int i=0; i < childs.size() && !found; i++) {

        resultActivity = findActivityById(childs.get(i), id);

        found = resultActivity != null && (resultActivity.getId() == id); //VERIFICAMOS CON EL "resultActivity != null"
                                                                          // QUE HAYA CONTENIDO antes de pillar la id con "resultActivity.getId() == id"
      }

    } else {

      resultActivity = new Project("Elemento no encontrado");

    }

    return resultActivity;

  }
}




