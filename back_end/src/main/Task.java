package main;



import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*La clase Task hereda atributos y métodos de la clase Activity, esto se debe al patrón
  COMPOSITE que hemos implementado, las tareas son la unidad mínima de actividades que
  podemos tener en nuestro programa, los proyectos serán los padres de las mismas */

public class Task extends Activity {

  static Logger loggerM1 = LoggerFactory.getLogger("timetracker.m1." + Task.class.getName());
  static Logger loggerM2 = LoggerFactory.getLogger("timetracker.m2." + Task.class.getName());

  protected boolean invariant() {
    loggerM2.trace("Initialising invariant conditions");
    return getName() != null && clock != null && getParent() != null
        && !getDuration().isNegative() && getIntervalList() != null;
  }

  public void setIntervaList(ArrayList<Interval> intervalList) {
    loggerM1.trace("Setting list of intervals");
    this.intervaList = intervalList;
  }

  private ArrayList<Interval> intervaList =  new ArrayList<>();
  private Clock clock;
  private Interval interval;
  private boolean active;

  public Task(String name, Clock clock) {
    super(name);
    loggerM1.info("Task named {} created", name);
    this.active = false;
    this.clock = clock;

    loggerM1.debug("Task name: {}", name);

  }

  public Task(String name, Clock clock, long id) {
    super(name, id);
    loggerM1.info("Task named {} created", name);
    this.active = false;
    this.clock = clock;

    loggerM1.debug("Task name: {}", name);

  }


  public boolean isActive() {
    loggerM1.info("Active Task running");
    return active;
  }

  private boolean assertHelper(boolean condition, String warningMsg, String correctMsg) {

    if (condition) {
      loggerM2.info(correctMsg);
    } else {
      loggerM2.warn(warningMsg);
    }

    return condition;
  }

  public Task(String name, Project activityParent, LocalDateTime iniDateTime,
              LocalDateTime finalDateTime, Duration duration, ArrayList<Interval> intervalList) {
    super(name, activityParent, iniDateTime, finalDateTime, duration);
    loggerM1.info("Task named {} created", name);

    loggerM1.debug("Task name: {}", name);
    loggerM1.debug("Task parent: {}", activityParent.getName());
    loggerM1.debug("Task initial date: {}", iniDateTime);
    loggerM1.debug("Task final date: {}", finalDateTime);
    loggerM1.debug("Task duration: {}", duration);

    this.intervaList = intervalList;
  }

  public Task(String name, Project activityParent, LocalDateTime iniDateTime,
              LocalDateTime finalDateTime, Duration duration) {
    super(name, activityParent, iniDateTime, finalDateTime, duration);
    loggerM1.info("Task named {} created", name);

    loggerM1.debug("Task name: {}", name);
    loggerM1.debug("Task parent: {}", activityParent.getName());
    loggerM1.debug("Task initial date: {}", iniDateTime);
    loggerM1.debug("Task final date: {}", finalDateTime);
    loggerM1.debug("Task duration: {}", duration);

  }

  public ArrayList<Interval> getIntervalList() {
    return intervaList;
  }

  public Task(String name, Project activityFather, ArrayList<Interval> intervalList) {
    super(name, activityFather);
    loggerM1.info("Task named {} created", name);
    this.intervaList = intervalList;

  }

  /*Método que inicializa un intervalo para nuestra tarea, se controla que
  el intervalo no se pueda crear si ya hay alguno ejecutándose */

  public void start() {

    loggerM1.info("Starting an Interval for task {}...", this.getName());
    assert assertHelper(invariant(), "Invariant condition not accomplished",
        "Initial conditions accomplished");

    //Pre-conditions

    assert assertHelper(!active, "Another Interval might be currently active",
        "No issues regarding existing active Intervals");

    interval = new Interval(this, this.clock);
    intervaList.add(interval);
    int countPre = clock.countObservers();
    clock.addObserver(interval);
    int countPost = clock.countObservers();
    active = true;

    //Post-conditions
    assert assertHelper((countPost == countPre + 1), "Observer might not been added correctly",
        "Interval starting check OK [1/4]");
    assert assertHelper(!intervaList.isEmpty(), "Observer might not been added correctly",
        "Interval starting check OK [2/4]");
    assert assertHelper(active, "Interval might not been activated correctly",
        "Interval starting check OK [3/4]");
    assert assertHelper(invariant(), "Invariant post-conditions not accomplished",
        "Interval starting check OK [4/4]");

    if ((countPost == countPre + 1) && !intervaList.isEmpty() && active && invariant()) {
      loggerM1.info("Interval starting for task {} successfully completed", this.getName());
    }
  }

  /*Método que nos permite parar los intervalos cuando dichas tareas o proyectos se paren,
    esto es crucial para no asignar tiempos y fechas incorrectas ya que
    si no se parasen los intervalos, seguirán sumandose segundos a estos */

  public void stop() {
    loggerM1.info("Starting an Interval for task {}...", this.getName());
    assert assertHelper(invariant(), "Invariant condition not accomplished",
        "Initial conditions accomplished");
    //Pre-conditions

    assert assertHelper(active, "Seems there is no interval currently active",
        "No issues regarding existing active intervals");

    int countPre = clock.countObservers();
    if (active) {
      clock.deleteObserver(interval);
      active = false;
    }
    int countPost = clock.countObservers();

    //Post-conditions

    assert assertHelper((countPost == countPre - 1), "Observer might not been removed correctly",
        "Interval stopping check OK [1/3]");

    assert assertHelper(!active, "There is no interval active to stop",
        "Interval stopping check OK [2/3]");

    assert assertHelper(invariant(), "Invariant condition not accomplished",
        "Interval stopping check OK [3/3]");

    if ((countPost == countPre - 1) && !active && invariant()) {
      loggerM1.info("Interval stopping for task {} successfully completed", this.getName());
    }

  }

  /*Declaración de la función necesaria para printar por pantalla,
  se implementa en la clase Project */
  @Override
  protected void readInfo(LocalDateTime initialDate, LocalDateTime finalDate, long seconds) {
    loggerM1.info("Reading {} task's time info...", this.getName());

    assert assertHelper(invariant(), "Invariant condition not accomplished",
        "Initial conditions accomplished");

    //Pre-conditions

    assert assertHelper(initialDate != null, "Interval's initial date not found",
        "Interval's initial date OK");

    boolean isFormatMatching = initialDate.format(formatter).matches(
        "([0-9]{4})-([0-9]{2})-([0-9]{2})([ ])([0-9]{2}):([0-9]{2}):([0-9]{2})");

    assert assertHelper(isFormatMatching, "Interval's initial date not matching the correct format",
        "Interval's initial date matches correctly the format");

    isFormatMatching = finalDate.format(formatter).matches(
        "([0-9]{4})-([0-9]{2})-([0-9]{2})([ ])([0-9]{2}):([0-9]{2}):([0-9]{2})");

    assert assertHelper(isFormatMatching, "Interval's final date not matching the correct format",
        "Interval's final date matches correctly the format");

    assert assertHelper(initialDate.isBefore(finalDate),
        "Dates' information is incoherent (initial date is nearer than the final)",
        "Dates' information is coherent between them");


    assert assertHelper(seconds >= 0, "The seconds counter is at negative values",
        "The seconds counter has a coherent value");


    if (active) {
      if (this.getInitialDate() == null) {
        this.setInitialDate(initialDate);
      }

      this.setFinalDate(finalDate);

      this.setDuration(this.getDuration().plus(Duration.ofSeconds(seconds)));

      System.out.format("%-16s %-19s %-25s %-25s %-10s\n", this.getName(),
          this.getParent().getName(), this.getInitialDate().format(formatter),
          this.getFinalDate().format(formatter),
          this.getDuration().toSecondsPart());

      this.getParent().readInfo(this.getInitialDate(), this.getFinalDate(), seconds);

    }

    //Post-conditions

    assert assertHelper(getInitialDate() != null, "Interval's initial date is Null",
        "Interval's initial date not Null");
    assert assertHelper(getFinalDate() != null, "Interval's final date is Null",
        "Interval's final date not Null");

    isFormatMatching = getInitialDate().format(formatter).matches(
        "([0-9]{4})-([0-9]{2})-([0-9]{2})([ ])([0-9]{2}):([0-9]{2}):([0-9]{2})");

    assert assertHelper(isFormatMatching, "Interval's initial date not matching the correct format",
        "Interval's initial date matches correctly the format");

    isFormatMatching = getFinalDate().format(formatter).matches(
        "([0-9]{4})-([0-9]{2})-([0-9]{2})([ ])([0-9]{2}):([0-9]{2}):([0-9]{2})");

    assert assertHelper(isFormatMatching, "Interval's final date not matching the correct format",
        "Interval's final date matches correctly the format");

    assert assertHelper(getDuration() != null && !getDuration().isNegative(),
        "Interval's duration is null or negative",
        "Interval's duration not null and has a positive value");

    assert assertHelper(invariant(), "Invariant post-conditions not accomplished",
        "Interval's info reading conditions OK");

    if (isFormatMatching && getDuration() != null && !getDuration().isNegative() && invariant()) {
      loggerM1.info("Reading {} task's time info finished successfully", this.getName());
    }

  }


  @Override
  public void accept(Visitor visitor, LocalDateTime ini, LocalDateTime end) {
    visitor.searchTimeSpend(this,  ini, end);
  }

  @Override
  public void accept(Visitor visitor, String tag) {
    visitor.searchTag(this, tag);
  }

  @Override
  public void accept(Visitor visitor, long id) { visitor.findActivityById(this, id); }

}
