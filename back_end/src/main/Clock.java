package main;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*La clase Clock se implementa como la parte observable del patrón OBSERVER,
  se dedica a contabilizar el tiempo como un reloj común y la clase Interval
  (que es el observer) visitará Clock para saber la hora en el momento que este deba actualizarla */
public class Clock extends Observable {

  private LocalDateTime currentDateTime;
  private final Timer timerTask;

  static Logger logger = LoggerFactory.getLogger("timetracker.m1." + Clock.class.getName());

  public Clock(String hour) {
    logger.trace("Entering constructor Clock(String hour)");

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    this.currentDateTime = LocalDateTime.parse(hour, formatter);
    timerTask = new Timer();
  }

  public Clock() {
    logger.trace("Entering default constructor of Clock");

    this.currentDateTime = LocalDateTime.now();
    this.timerTask = new Timer();
  }

  public Timer getTimerTask() {
    logger.info("Getting TimerTask on Clock class");
    return timerTask;
  }

  public LocalDateTime getCurrentDateTime() {
    logger.info("Getting CurrentDateTime on Clock class");
    return currentDateTime;
  }

  /*Esta función sirve para notificar la hora deseada a la clase main.Interval.
  De esta forma la clase observer obtendrá la hora para así actualizarla y ser
  "asiganada" a las actividades correspondientes */

  public void tick() {
    logger.trace("Entering on function Tick");
    this.currentDateTime = LocalDateTime.now();
    setChanged();
    notifyObservers(currentDateTime);
  }

  /*Función que inicializa el reloj (cada 2s) con el formato indicado HH:MM:SS */
  public void startClock(int period) {
    logger.trace("Initialising Clock with correct format");
    getTimerTask().schedule(new TimerTask() {
        @Override
        public void run() {
          tick();
        }
    }, 2000, period * 1000);
  }
}
