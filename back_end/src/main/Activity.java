package main;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Random;

/*La clase Activity es la "clase padre" de Project y Task, en estas tres clases
  se implementa el patrón COMPOSITE ya que Project y Task tienen
  múltiples atributos compartidos al igual que getters y setters.
  También deben poder leer la información mediante la función abstracta readInfo(...)*/
public abstract class Activity implements Visitable {

  private String name;
  private long id = 0;
  private LocalDateTime initialDate;
  private LocalDateTime finalDate;
  private Duration duration = Duration.ZERO;
  private Activity parent;
  protected DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
  private ArrayList<String> tagList = new ArrayList<>();

  static Logger logger = LoggerFactory.getLogger("timetracker.m1." + Activity.class.getName());

  public Activity(String name) {
    logger.trace("Entering constructor Activity(String name)");

    this.name = name;
    this.initialDate = null;
    this.finalDate = null;
    this.parent = null;
    this.id = generateId();
  }

  public Activity(String name, long id) {
    logger.trace("Entering constructor Activity(String name, int id)");

    this.name = name;
    this.initialDate = null;
    this.finalDate = null;
    this.parent = null;
    this.id = id;
  }

  public Activity(String value, Activity parent, LocalDateTime initialDate,
                  LocalDateTime finalDate, Duration duration) {
    logger.trace("Entering constructor Activity(String value, Activity parent, "
        + "LocalDateTime initialDate,\n"
        + "LocalDateTime finalDate, Duration duration)");
    this.name = value;
    this.parent = parent;
    this.initialDate = initialDate;
    this.finalDate = finalDate;
    this.duration = duration;
  }

  public Activity(String name, Project activityFather) {
    logger.trace("Entering constructor Activity(String name, "
        + "Project activityFather)");
    this.name = name;
    this.parent = activityFather;
  }

  public long getId() { return id; }

  public String getName() {
    return name;
  }

  public Activity getParent() {
    return parent;
  }

  public LocalDateTime getInitialDate() {
    if(initialDate == null){
      return  null; // TODO controloar cuando es null
    }
    return initialDate;
  }

  public LocalDateTime getFinalDate() {

    return finalDate; // TODO controloar cuando es null

  }

  public Duration getDuration() {
    return duration;
  }

  public ArrayList<String> getTagList() {
    return tagList;
  }

  public void addTag(String tag) {
    //TODO CONTROLAR MAYUSCULAS
    if (!this.tagList.contains(tag)) {
      this.tagList.add(tag);
    }
  }

  private long generateId () {
    Random random = new Random();

    //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yy.MM.dd.HH.mm.ss");
    //String initialTemp = LocalDateTime.now().format(formatter);
    //initialTemp = initialTemp.replaceAll("[.]*", "");

    int randomId = random.nextInt(2147483647);
    //System.out.println(randomId);

    return randomId;
  }

  
  public void setInitialDate(LocalDateTime initialDate) {
    this.initialDate = initialDate;
  }

  public void setFinalDate(LocalDateTime finalDate) {
    this.finalDate = finalDate;
  }

  public void setDuration(Duration duration) {
    this.duration = duration;
  }

  public void setParent(Activity parent) {
    this.parent = parent;
  }

  public void setTagList(ArrayList<String> tagList) {
    this.tagList = tagList;
  }

  //Declaración abstracta del método readInfo que heredarán las clases Project y Task
  protected abstract void readInfo(LocalDateTime initialDate,
                                   LocalDateTime finalDate, long seconds);

  public abstract void accept(Visitor visitor, String name);

  public abstract void accept(Visitor visitor, long id);

  public abstract void accept(Visitor visitor, LocalDateTime initialDate, LocalDateTime finalDate);

}
