package webserver;


import main.*;


public class MainWebServer {

  public int id = 0;

  public static void main(String[] args) {
    webServer();
  }

  public static void webServer() {
    //TODO crear el arbol

    Clock clock = new Clock();
    clock.startClock(2);

    final Activity root = makeTreeCourses(clock);
    // implement this method that returns the tree of
    // appendix A in the practicum handout

    // start your clock

    new WebServer(root);

  }

  private static Activity makeTreeCourses(Clock clock) {


    Project proot = new Project("ROOT", 0, clock);

    //HIJOS DE ROOT
    Project psoftDesign = new Project("Software Design");
    Project psoftTesting = new Project("Software Testing");
    Project pdataBase = new Project("Databases");
    Task ttransport = new Task("transportation", clock);

    proot.addActivity(psoftDesign);
    proot.addActivity(psoftTesting);
    proot.addActivity(pdataBase);
    proot.addActivity(ttransport);

    //HIJOS DE SOFTWARE DESIGN
    Project pproblems = new Project("Problems");
    Project ptimeTracker = new Project("Time Tracker");

    psoftDesign.addActivity(pproblems);
    psoftDesign.addActivity(ptimeTracker);


    //HIJOS DE PROBLEMS
    Task tfirstList = new Task("First List", clock);
    Task tsecondList = new Task("Second List", clock);

    pproblems.addActivity(tfirstList);
    pproblems.addActivity(tsecondList);

    //HIJOS DE TIME TRACKER
    Task treadHandout = new Task("Read handout", clock);
    Task tfirstMilestone = new Task("First Milestone", clock);

    ptimeTracker.addActivity(treadHandout);
    ptimeTracker.addActivity(tfirstMilestone);

    return proot;

  }
}